import { useState, useEffect } from "react";
import axios from "axios";

function useGeoCords(city, countryCode) {
    const [lat, setLat] = useState();
    const [lon, setLon] = useState();

    const apiKey = import.meta.env.REACT_APP_WEATHER_API_KEY

    const queryParams = {params: {
            q: `${city},${countryCode}`,
            appid: apiKey
        }}

    useEffect(() => {
        const getGeoCords = async () => {
            try {
                const response = await axios.get(
                    `https://api.openweathermap.org/geo/1.0/direct`, queryParams
                );
                setLat(response.data[0].lat);
                setLon(response.data[0].lon);
            } catch (error) {
                console.error(error);
            }
        };
        getGeoCords();
    }, [city, countryCode, apiKey]);

    return {lat, lon};
}

export default useGeoCords;