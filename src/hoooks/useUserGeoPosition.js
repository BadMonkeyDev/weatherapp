import {useEffect, useState} from "react";

export function useUserGeoPosition() {
    const [position, setPosition] = useState({ lat: null, lon: null });

    useEffect(() => {
        if (!navigator.geolocation) {
            console.log("Geolocation is not supported by your browser");
            return;
        }

        navigator.geolocation.getCurrentPosition(
            (position) => {
                setPosition({
                    lat: position.coords.latitude,
                    lon: position.coords.longitude,
                });
            },
            (error) => console.log(error)
        );
    }, []);

    return position;
}