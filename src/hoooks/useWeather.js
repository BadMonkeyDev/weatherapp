import {useEffect, useState} from 'react';
import axios from "axios";

export const useWeather = (lat, lon) => {
    const apiKey = import.meta.env.REACT_APP_WEATHER_API_KEY
    const requestConfig = {params: {lat, lon, appid: apiKey, units: 'metric', lang: 'ua'}}
    const [weatherData, setWeatherData] = useState(null)

    useEffect(() => {
        const fetchWeather = async () => {
            try {
                const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather`, requestConfig)
                return JSON.stringify(response.data, null, 4)
            } catch (error) {
                console.error(error);
            }
        };
        fetchWeather().then(data => setWeatherData(data))
    }, [])
    return weatherData;
};