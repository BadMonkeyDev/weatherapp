import {useWeather} from "../hoooks/useWeather.js";

export const WeatherData = ({lat, lon}) => {
    let weather = useWeather(lat, lon)

    return (
        <div className="code">
            <pre>
                {weather}
            </pre>
        </div>
    );
};