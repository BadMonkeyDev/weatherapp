import React from 'react';
import useGeoCords from "../hoooks/useGeoCords.js";
import {WeatherData} from "./WeatherData.jsx";

const WeatherPage = () => {
    let {lat, lon} = useGeoCords('Полтава', 'UA')

    return (
        <div>
            {lat && lon ? <WeatherData lat={lat} lon={lon} /> : 'Loading...'}
        </div>
    );
};

export default WeatherPage;