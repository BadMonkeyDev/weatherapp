import React from 'react';
import dotenv from 'dotenv'
import './App.css'
import WeatherPage from "./components/WeatherPage.jsx";

const App = () => {
    return (
        <div>
            <WeatherPage />
        </div>
    );
};

export default App;