<h1>Weather App</h1>
<p>This project is a weather application that provides users with current weather information for a selected location. The application uses OpenWeatherMap API to retrieve live weather data.</p>

<h2>Features</h2>

- [new]Ability to search for weather information by city name
- [new]Display current weather information, such as temperature, humidity, and wind speed
- [in progress]Option to switch between metric and imperial units
- [in progress]Option to save frequently searched locations for quick access in the future

<h2>Technology Stack</h2>
- ReactJS for front-end development
- Node.js for back-end development
- OpenWeatherMap API for weather data

<h2>Setup</h2>
<h3>To run the app locally, follow these steps:</h3>
1. Clone the repository:

```bash
git clone https://gitlab.com/BadMonkeyDev/weatherapp.git
```


2. Navigate to the project directory:
```bash
cd weatherapp
```

3. Install the required packages:
```bash
npm install
```

4. Start the development server:
```bash
npm start
```
<details><summary>Note: You need to have OpenWeatherMap API key stored in .env file in project repository to make API requests</summary>

```dotenv
REACT_APP_WEATHER_API_KEY=yourapikey
```
</details>
<i>P.S. If you dont have one yet, go to https://openweathermap.org/ register and generate api key</i>
<br><br>
<p>The application should now be running on http://localhost:3000.</p>

<h2>Contribution</h2>
<h3>This project is open to contributions and suggestions. If you would like to contribute, please follow these steps:</h3>

- Fork the repository
- Create a new branch for your changes
- Make the desired changes
- Commit your changes and push to your fork
- Create a pull request to the main repository.

<h2>License</h2>
<p>This project is licensed under the MIT License. See LICENSE for more details.</p>
